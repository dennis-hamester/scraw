/* Copyright (c) 2016, Dennis Hamester <dennis.hamester@startmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <scraw/scraw.h>
#include <assert.h>
#include <signal.h>
#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_CTRLS 64

typedef struct {
    scraw_controller_t* scraw_ctrl;
    scraw_controller_info_t info;
    scraw_controller_config_t cfg;
    scraw_controller_state_t state;
} controller;

static controller* ctrls[MAX_CTRLS];
static atomic_bool running = ATOMIC_VAR_INIT(true);

static void signal_handler(int sig) {
    if(!atomic_exchange(&running, false)) {
        abort();
    }
}

static struct {
    uint32_t btn;
    const char* name;
} button_names[] = {
    { SCRAW_BTN_RIGHT_GRIPPER, "Right Gripper" },
    { SCRAW_BTN_LEFT_TP, "Left Trackpad" },
    { SCRAW_BTN_RIGHT_TP, "Right Trackpad" },
    { SCRAW_BTN_LEFT_TP_TOUCH, "Left Trackpad Touch" },
    { SCRAW_BTN_RIGHT_TP_TOUCH, "Right Trackpad Touch" },
    { SCRAW_BTN_JOYSTICK, "Joystick" },
    { SCRAW_BTN_LEFT_TP_UP, "Left Trackpad Up" },
    { SCRAW_BTN_LEFT_TP_RIGHT, "Left Trackpad Right" },
    { SCRAW_BTN_LEFT_TP_LEFT, "Left Trackpad Left" },
    { SCRAW_BTN_LEFT_TP_DOWN, "Left Trackpad Down" },
    { SCRAW_BTN_SELECT, "Select" },
    { SCRAW_BTN_STEAM, "Steam" },
    { SCRAW_BTN_START, "Start" },
    { SCRAW_BTN_LEFT_GRIPPER, "Left Gripper" },
    { SCRAW_BTN_RIGHT_TRIGGER, "Right Trigger" },
    { SCRAW_BTN_LEFT_TRIGGER, "Left Trigger" },
    { SCRAW_BTN_RIGHT_SHOULDER, "Right Shoulder" },
    { SCRAW_BTN_LEFT_SHOULDER, "Left Shoulder" },
    { SCRAW_BTN_Y, "Y" },
    { SCRAW_BTN_B, "B" },
    { SCRAW_BTN_X, "X" },
    { SCRAW_BTN_A, "A" },
    { 0, NULL },
};

static inline bool button_diff(uint32_t btn, const scraw_controller_state_t* a, const scraw_controller_state_t* b) {
    return (a->buttons & btn) ^ (b->buttons & btn);
}

static inline const char* button_str(uint32_t btn, const scraw_controller_state_t* state) {
    if(state->buttons & btn) {
        return "Pressed";
    }
    else {
        return "Released";
    }
}

static void on_state_change(scraw_controller_t* scraw_ctrl, const scraw_controller_state_t* new) {
    controller* ctrl;
    scraw_controller_get_user_data(scraw_ctrl, (void**)&ctrl);
    scraw_controller_state_t* old = &ctrl->state;

    for(int i = 0; button_names[i].btn; ++i) {
        if(button_diff(button_names[i].btn, old, new)) {
            printf("Controller %s --- %s: %s\n", ctrl->info.serial_number, button_names[i].name, button_str(button_names[i].btn, new));
        }
    }

    if(old->left_trigger != new->left_trigger) {
        printf("Controller %s --- Left Trigger: %6hi\n", ctrl->info.serial_number, new->left_trigger);
    }

    if(old->right_trigger != new->right_trigger) {
        printf("Controller %s --- Right Trigger: %6hi\n", ctrl->info.serial_number, new->right_trigger);
    }

    if((old->left_trackpad_x != new->left_trackpad_x) || (old->left_trackpad_y != new->left_trackpad_y)) {
        printf("Controller %s --- Left Trackpad: %6hi %6hi\n", ctrl->info.serial_number, new->left_trackpad_x, new->left_trackpad_y);
    }

    if((old->right_trackpad_x != new->right_trackpad_x) || (old->right_trackpad_y != new->right_trackpad_y)) {
        printf("Controller %s --- Right Trackpad: %6hi %6hi\n", ctrl->info.serial_number, new->right_trackpad_x, new->right_trackpad_y);
    }

    if((old->joystick_x != new->joystick_x) || (old->joystick_y != new->joystick_y)) {
        printf("Controller %s --- Joystick: %6hi %6hi\n", ctrl->info.serial_number, new->joystick_x, new->joystick_y);
    }

    if((old->d_imu_pitch != new->d_imu_pitch) ||
       (old->d_imu_roll != new->d_imu_roll) ||
       (old->d_imu_yaw != new->d_imu_yaw)) {
        printf("Controller %s --- Delta IMU: %6hi %6hi %6hi\n", ctrl->info.serial_number, new->d_imu_pitch, new->d_imu_roll, new->d_imu_yaw);
    }

    memcpy(old, new, sizeof(scraw_controller_state_t));
}

static void configure_controller(controller* ctrl, bool lizard, uint16_t idle_timeout, bool imu) {
    scraw_controller_config_t cfg = {
        .size = sizeof(scraw_controller_config_t),
        .idle_timeout = idle_timeout,
        .imu = imu,
    };
    memcpy(&ctrl->cfg, &cfg, cfg.size);
    scraw_controller_configure(ctrl->scraw_ctrl, &cfg);
    printf("Controller %s --- Idle timeout set to %hu seconds\n", ctrl->info.serial_number, idle_timeout);
    if(imu) {
        printf("Controller %s --- IMU enabled\n", ctrl->info.serial_number);
    }
    else {
        printf("Controller %s --- IMU disabled\n", ctrl->info.serial_number);
    }

    uint8_t audio_indices[16];
    memset(audio_indices, 0xff, sizeof(audio_indices));
    scraw_controller_set_audio_indices(ctrl->scraw_ctrl, audio_indices);

    scraw_controller_lizard_buttons(ctrl->scraw_ctrl, lizard);
    if(lizard) {
        scraw_controller_enable_lizard_analog(ctrl->scraw_ctrl);
        printf("Controller %s --- Lizard mode enabled\n", ctrl->info.serial_number);
    }
    else {
        printf("Controller %s --- Lizard mode disabled\n", ctrl->info.serial_number);
    }
}

static void print_controller_info(controller* ctrl) {
    time_t tmp;

    printf(" Product ID: %d\n", (int)ctrl->info.product_id);
    tmp = ctrl->info.bootloader_ts;
    printf(" Bootloader Build Time: %s", ctime(&tmp));
    tmp = ctrl->info.firmware_ts;
    printf(" Firmware Build Time: %s", ctime(&tmp));
    tmp = ctrl->info.radio_ts;
    printf(" Radio Build Time: %s", ctime(&tmp));
    printf(" Serial Number: %s\n", ctrl->info.serial_number);

    if(ctrl->info.type == SCRAW_CONTROLLER_TYPE_WIRELESS) {
        tmp = ctrl->info.receiver_firmware_ts;
        printf(" Receiver Firmware Build Time: %s", ctime(&tmp));
        printf(" Receiver Serial Number: %s\n", ctrl->info.receiver_serial_number);
    }
}

static void on_controller_gained(scraw_controller_t* scraw_ctrl) {
    controller* ctrl = malloc(sizeof(controller));
    memset(ctrl, 0, sizeof(controller));
    ctrl->scraw_ctrl = scraw_ctrl;

    scraw_controller_retain(scraw_ctrl);

    int type;
    scraw_controller_get_type(scraw_ctrl, &type);

    printf("\nNew ");
    if(type == SCRAW_CONTROLLER_TYPE_WIRED) {
        printf("wired");
    }
    else {
        printf("wireless");
    }
    printf(" controller\n");

    ctrl->info.size = sizeof(scraw_controller_info_t);
    scraw_controller_get_info(scraw_ctrl, &ctrl->info);
    assert(ctrl->info.size == sizeof(scraw_controller_info_t));

    for(int i = 0; i < MAX_CTRLS; ++i) {
        if(!ctrls[i]) {
            ctrls[i] = ctrl;
            break;
        }
    }

    print_controller_info(ctrl);
    printf("\n");
    configure_controller(ctrl, false, 300, false);
    scraw_controller_set_user_data(scraw_ctrl, ctrl);
    scraw_controller_on_state_change(scraw_ctrl, on_state_change);
}

static void on_controller_lost(scraw_controller_t* scraw_ctrl) {
    controller* ctrl;
    for(int i = 0; i < MAX_CTRLS; ++i) {
        if(ctrls[i] && (ctrls[i]->scraw_ctrl == scraw_ctrl)) {
            ctrl = ctrls[i];
            ctrls[i] = NULL;
            break;
        }
    }

    printf("\nLost ");
    if(ctrl->info.type == SCRAW_CONTROLLER_TYPE_WIRED) {
        printf("wired");
    }
    else {
        printf("wireless");
    }
    printf(" controller (%s)\n\n", ctrl->info.serial_number);

    free(ctrl);
    scraw_controller_release(scraw_ctrl);
}

static void on_scraw_event(scraw_context_t* ctx, int reason, void* data, void* user_data) {
    if(reason == SCRAW_EVENT_CONTROLLER_GAINED) {
        on_controller_gained(data);
    }
    else if(reason == SCRAW_EVENT_CONTROLLER_LOST) {
        on_controller_lost(data);
    }
}

int main(int argc, char* argv[]) {
    printf("scraw_info %d.%d.%d\n", SCRAW_VERSION_MAJOR, SCRAW_VERSION_MINOR, SCRAW_VERSION_PATCH);
    printf("Exit by pressing CTRL-C.\n");

    scraw_context_t* ctx;
    if(scraw_context_create(&ctx, on_scraw_event, ctrls) != SCRAW_RESULT_SUCCESS) {
        fprintf(stderr, "Failed to create context\n");
        return 1;
    }

    signal(SIGTERM, signal_handler);
    signal(SIGINT, signal_handler);

    while(atomic_load(&running)) {
        scraw_context_handle_events(ctx, 100);
    }

    printf("\n");
    for(int i = 0; i < MAX_CTRLS; ++i) {
        if(ctrls[i]) {
            configure_controller(ctrls[i], true, ctrls[i]->cfg.idle_timeout, ctrls[i]->cfg.imu);
        }
    }

    scraw_context_destroy(ctx);
    return 0;
}
