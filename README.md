Overview
========
`scraw` is a C library for the Steam Controller. The current version is 0.2.1.

It provides low-level almost raw access to the controller. The ultimate goal is
to develop an open-source and stand-alone alternative to the Steam Controller
functionality that is present today in the official Steam client. This project
is not in any way affiliated with Valve Cooperation.

A small utility called `scraw_info` is bundled with this library that allows you
to test `scraw` and shows you how to use the library.

At the moment, only Linux is supported.

Contributions are [very welcome](CONTRIBUTING.md)


API Reference
=============
The API reference for the current master branch is hosted here:
https://dennis-hamester.gitlab.io/scraw/

A protocol description is available here:
https://dennis-hamester.gitlab.io/scraw/protocol/


Features
========
* Fully asynchronous hot-plugging.
* Asynchronous and synchronous access to all discrete buttons, track pads, the
  joystick as well as the IMU (inertial measurement unit).
* Enable and disable lizard mode.
* Serial number and build timestamps for controller and receiver can be queried.
* Simultaneous use of the left track pad and joystick.
* IMU can be enabled and disabled.
* Idle timeout after which the wireless controller automatically turns off can
  be configured.
* Haptic feedback supported.
* LED brightness can be controlled.
* Turn on and off sound can be changed.
* Various other audio tunes can be played at random.
* Controller calibration (track pads, joystick and IMU)


Non-Features
============
* The purpose of this library is just to access all the information the
  controller provides. It does not emulate any virtual devices or control your
  mouse cursor, like the official Steam client.

* `scraw` does **not** allow you to make changes to the internal controller or
  receiver firmware. I haven't even tried to figure this out. I don't want
  anyone to brick their controller.


Limitations
===========
Some of these limitations are actually caused be the controller and or receiver
hardware itself and not `scraw`. At at least assume that to be the case when the
official Steam client has the same problem.

* `scraw` can not properly detect when the left track pad is pressed without
  touching it. This might sound weird, but it happens when you press the track
  pad with something other than your finger. This has to do with parts of the
  interrupt packets being reused for the left track pad and joystick. The right
  track pad does not have this limitation.

* The battery charge level is supposedly transmitted somewhere. I have yet to
  figure out where exactly. So, this information is not available for now.

* Lizard mode doesn't work properly with the wireless controller. Or to be more
  precise: it works only while some application actively queries the controller
  state. The official Steam client has the problem as far as I can tell.


Compiling
=========
`scraw` depends on [libusb](http://libusb.info/) version 1.16.0. Additionally,
[CMake](https://cmake.org/) (3.1) is required to build the library.

```shell
mkdir build
cd build
cmake ..
make
```

This will build just the library in release mode. The documentation will be
built if [Doxygen](http://www.stack.nl/~dimitri/doxygen/index.html) can be
found. You can turn that off with the CMake variable `BUILD_DOCS`.

The utility `scraw_info` can be built by enable `BUILD_SCRAW_INFO`.

```shell
mkdir build
cd build
cmake -DBUILD_SCRAW_INFO=ON ..
make
```

Use `make install` to install `scraw`. The installation prefix can be changed
with `CMAKE_INSTALL_PREFIX`.


Packages
========
* Arch Linux: https://aur.archlinux.org/packages/scraw/
* Arch Linux (git version): https://aur.archlinux.org/packages/scraw-git/


License
=======
`scraw` is distributed under the ISC license. This project is not affiliated
with Valve Cooperation.

```
Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
```
