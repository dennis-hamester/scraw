/* Copyright (c) 2016, Dennis Hamester <dennis.hamester@startmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef _SCRAW_PACKETS_H_
#define _SCRAW_PACKETS_H_

#include <stdint.h>
#include <assert.h>
#include <stdalign.h>

typedef enum {
    PACKET_TYPE_STATE = 0x01,
    PACKET_TYPE_DISABLE_LIZARD_BUTTONS = 0x81,
    PACKET_TYPE_ID = 0x83,
    PACKET_TYPE_CONFIGURE = 0x87,
    PACKET_TYPE_ENABLE_LIZARD_BUTTONS = 0x85,
    PACKET_TYPE_ENABLE_LIZARD_AXES = 0x8e,
    PACKET_TYPE_FEEDBACK = 0x8f,
    PACKET_TYPE_RESET = 0x95,
    /* PACKET_TYPE_UNKNOWN_96 = 0x96, */
    PACKET_TYPE_OFF = 0x9f,
    PACKET_TYPE_RECEIVER_ID = 0xa1,
    PACKET_TYPE_CALIBRATE_TRACKPAD = 0xa7,
    /* PACKET_TYPE_UNKNOWN_AD = 0xad, */
    PACKET_TYPE_SERIAL_NUMBER = 0xae,
    PACKET_TYPE_PROBE = 0xb4,
    PACKET_TYPE_CALIBRATE_IMU = 0xb5,
    PACKET_TYPE_AUDIO = 0xb6,
    /* PACKET_TYPE_UNKNOWN_BA = 0xba, */
    PACKET_TYPE_CALIBRATE_JOYSTICK = 0xbf,
    PACKET_TYPE_SET_AUDIO_INDICES = 0xc1,
} packet_type;

typedef enum {
    PACKET_STATE_ST_UPDATE = 0x01,
    PACKET_STATE_ST_HOTPLUG = 0x03,
    PACKET_STATE_ST_SEQ = 0x04,
} packet_state_sub_type;

typedef enum {
    PACKET_SERIAL_NUMBER_ID_MAINBOARD = 0x00,
    PACKET_SERIAL_NUMBER_ID_CONTROLLER = 0x01,
} packet_serial_number_id;

typedef enum {
    PACKET_PROBE_STATUS_DISCONNECTED = 0x01,
    PACKET_PROBE_STATUS_CONNECTED = 0x02,
} packet_probe_status;

typedef struct {
    uint8_t seq_number[4];
    uint8_t buttons[3];
    uint8_t left_trigger;
    uint8_t right_trigger;
    uint8_t unknown_0d_0f[3];
    uint8_t left_analog_x[2];
    uint8_t left_analog_y[2];
    uint8_t right_trackpad_x[2];
    uint8_t right_trackpad_y[2];
    uint8_t left_trigger_full[2];
    uint8_t right_trigger_full[2];
    uint8_t unknown_1c_21[6];
    uint8_t d_imu_pitch[2];
    uint8_t d_imu_yaw[2];
    uint8_t d_imu_roll[2];
    uint8_t unknown_28_3f[24];
} packet_state_update;
static_assert(alignof(packet_state_update) == 1, "packet_state_update must be byte aligned");
static_assert(sizeof(packet_state_update) == 60, "packet_state_update must be 56 bytes long");

typedef struct {
    uint8_t status;
    uint8_t unused[59];
} packet_state_hotplug;
static_assert(alignof(packet_state_hotplug) == 1, "packet_state_hotplug must be byte aligned");
static_assert(sizeof(packet_state_hotplug) == 60, "packet_state_hotplug must be 56 bytes long");

typedef struct {
    uint8_t seq_number[4];
    uint8_t unknown_08_0b[4];
    uint8_t unknown_0c;
    uint8_t unknown_0d;
    uint8_t unknown_0e;
    uint8_t unused[49];
} packet_state_seq;
static_assert(alignof(packet_state_seq) == 1, "packet_state_seq must be byte aligned");
static_assert(sizeof(packet_state_seq) == 60, "packet_state_seq must be 56 bytes long");

typedef struct {
    uint8_t type;
    uint8_t unknown_01;
    uint8_t sub_type;
    uint8_t size;

    union {
        uint8_t data[60];
        packet_state_update update;
        packet_state_hotplug hotplug;
        packet_state_seq seq;
    };
} packet_state;
static_assert(alignof(packet_state) == 1, "packet_state must be byte aligned");
static_assert(sizeof(packet_state) == 64, "packet_state must be 64 bytes long");

typedef struct {
    uint8_t type;
    uint8_t size;
    uint8_t unused[62];
} packet_disable_lizard_buttons;
static_assert(alignof(packet_disable_lizard_buttons) == 1, "packet_disable_lizard_buttons must be byte aligned");
static_assert(sizeof(packet_disable_lizard_buttons) == 64, "packet_disable_lizard_buttons must be 64 bytes long");

typedef struct {
    uint8_t type;
    uint8_t size;
    uint8_t unknown_02_07[6];
    uint8_t product[2];
    uint8_t unknown_0a_11[8]; /* probably contains capabilities and board revision */
    uint8_t bootloader_ts[4];
    uint8_t unknown_16;
    uint8_t firmware_ts[4];
    uint8_t unknown_21;
    uint8_t radio_ts[4];
    uint8_t unknown_26_2a[5];
    uint8_t unused[27];
} packet_id;
static_assert(alignof(packet_id) == 1, "packet_id must be byte aligned");
static_assert(sizeof(packet_id) == 64, "packet_id must be 64 bytes long");

typedef struct {
    uint8_t timeout[2];
    uint8_t unknown_05_12[13];
    uint8_t enable_imu;
    uint8_t unknown_14_15[2];
    uint8_t unused[43];
} packet_configure_st_config;
static_assert(alignof(packet_configure_st_config) == 1, "packet_configure_st_config must be byte aligned");
static_assert(sizeof(packet_configure_st_config) == 61, "packet_configure_st_config must be 61 bytes long");

typedef struct {
    uint8_t led;
    uint8_t unknown_04;
    uint8_t unused[59];
} packet_configure_st_led;
static_assert(alignof(packet_configure_st_led) == 1, "packet_configure_st_led must be byte aligned");
static_assert(sizeof(packet_configure_st_led) == 61, "packet_configure_st_led must be 61 bytes long");

typedef struct {
    uint8_t type;
    uint8_t size;
    uint8_t unknown_02;
    union {
        uint8_t data[61];
        packet_configure_st_config cfg;
        packet_configure_st_led led;
    };
} packet_configure;
static_assert(alignof(packet_configure) == 1, "packet_configure must be byte aligned");
static_assert(sizeof(packet_configure) == 64, "packet_configure must be 64 bytes long");

typedef struct {
    uint8_t type;
    uint8_t size;
    uint8_t unused[62];
} packet_enable_lizard_buttons;
static_assert(alignof(packet_enable_lizard_buttons) == 1, "packet_enable_lizard_buttons must be byte aligned");
static_assert(sizeof(packet_enable_lizard_buttons) == 64, "packet_enable_lizard_buttons must be 64 bytes long");

typedef struct {
    uint8_t type;
    uint8_t size;
    uint8_t unused[62];
} packet_enable_lizard_axes;
static_assert(alignof(packet_enable_lizard_axes) == 1, "packet_enable_lizard_axes must be byte aligned");
static_assert(sizeof(packet_enable_lizard_axes) == 64, "packet_enable_lizard_axes must be 64 bytes long");

typedef struct {
    uint8_t type;
    uint8_t size;
    uint8_t side;
    uint8_t amplitude[2];
    uint8_t period[2];
    uint8_t count[2];
    uint8_t unknown_09;
    uint8_t unused[54];
} packet_feedback;
static_assert(alignof(packet_feedback) == 1, "packet_feedback must be byte aligned");
static_assert(sizeof(packet_feedback) == 64, "packet_feedback must be 64 bytes long");

typedef struct {
    uint8_t type;
    uint8_t size;
    uint8_t unused[62];
} packet_reset;
static_assert(alignof(packet_reset) == 1, "packet_reset must be byte aligned");
static_assert(sizeof(packet_reset) == 64, "packet_reset must be 64 bytes long");

typedef struct {
    uint8_t type;
    uint8_t size;
    uint8_t off[4];
    uint8_t unused[58];
} packet_off;
static_assert(alignof(packet_off) == 1, "packet_off must be byte aligned");
static_assert(sizeof(packet_off) == 64, "packet_off must be 64 bytes long");

typedef struct {
    uint8_t type;
    uint8_t size;
    uint8_t firmware_ts[4];
    uint8_t serial_unknown[10];
    uint8_t serial[10];
    uint8_t unknown_1a_23[10];
    uint8_t unused[28];
} packet_receiver_id;
static_assert(alignof(packet_receiver_id) == 1, "packet_receiver_id must be byte aligned");
static_assert(sizeof(packet_receiver_id) == 64, "packet_receiver_id must be 64 bytes long");

typedef struct {
    uint8_t type;
    uint8_t size;
    uint8_t unused[62];
} packet_calibrate_trackpad;
static_assert(alignof(packet_calibrate_trackpad) == 1, "packet_calibrate_trackpad must be byte aligned");
static_assert(sizeof(packet_calibrate_trackpad) == 64, "packet_calibrate_trackpad must be 64 bytes long");

typedef struct {
    uint8_t type;
    uint8_t size;
    uint8_t id;
    char serial[10];
    uint8_t unused[51];
} packet_serial_number;
static_assert(alignof(packet_serial_number) == 1, "packet_serial_number must be byte aligned");
static_assert(sizeof(packet_serial_number) == 64, "packet_serial_number must be 64 bytes long");

typedef struct {
    uint8_t type;
    uint8_t size;
    uint8_t status;
    uint8_t unused[61];
} packet_probe;
static_assert(alignof(packet_probe) == 1, "packet_probe must be byte aligned");
static_assert(sizeof(packet_probe) == 64, "packet_probe must be 64 bytes long");

typedef struct {
    uint8_t type;
    uint8_t size;
    uint8_t unused[62];
} packet_calibrate_imu;
static_assert(alignof(packet_calibrate_imu) == 1, "packet_calibrate_imu must be byte aligned");
static_assert(sizeof(packet_calibrate_imu) == 64, "packet_calibrate_imu must be 64 bytes long");

typedef struct {
    uint8_t type;
    uint8_t size;
    uint8_t audio_type;
    uint8_t unknown_03_05[3];
    uint8_t unused[58];
} packet_audio;
static_assert(alignof(packet_audio) == 1, "packet_audio must be byte aligned");
static_assert(sizeof(packet_audio) == 64, "packet_audio must be 64 bytes long");

typedef struct {
    uint8_t type;
    uint8_t size;
    uint8_t unused[62];
} packet_calibrate_joystick;
static_assert(alignof(packet_calibrate_joystick) == 1, "packet_calibrate_joystick must be byte aligned");
static_assert(sizeof(packet_calibrate_joystick) == 64, "packet_calibrate_joystick must be 64 bytes long");

typedef struct {
    uint8_t type;
    uint8_t size;
    uint8_t indices[16];
    uint8_t unused[46];
} packet_set_audio_indices;
static_assert(alignof(packet_set_audio_indices) == 1, "packet_set_audio_indices must be byte aligned");
static_assert(sizeof(packet_set_audio_indices) == 64, "packet_set_audio_indices must be 64 bytes long");

typedef union {
    uint8_t type;
    uint8_t data[64];

    packet_state state;
    packet_disable_lizard_buttons disable_lizard_buttons;
    packet_id id;
    packet_configure disable_configure;
    packet_enable_lizard_buttons enable_lizard_buttons;
    packet_enable_lizard_axes enable_lizard_axes;
    packet_feedback feedback;
    packet_reset reset;
    packet_off off;
    packet_receiver_id receiver_id;
    packet_calibrate_trackpad calibrate_trackpad;
    packet_serial_number serial_number;
    packet_probe probe;
    packet_calibrate_imu calibrate_imu;
    packet_audio audio;
    packet_calibrate_joystick calibrate_joystick;
    packet_set_audio_indices set_audio_indices;
} packet;
static_assert(alignof(packet) == 1, "packet must be byte aligned");
static_assert(sizeof(packet) == 64, "packet must be 64 bytes long");

#define u32_from_packet_le(v) ((uint32_t)((v[3] << 24) | (v[2] << 16) | (v[1] << 8) | v[0]))
#define u32_from_packet_be(v) ((uint32_t)((v[0] << 24) | (v[1] << 16) | (v[2] << 8) | v[3]))
#define u16_from_packet_le(v) ((uint16_t)((v[1] << 8) | v[0]))
#define u16_to_packet_le(v) { (uint8_t)(v & 0x00FF), (uint8_t)((v & 0xFF00) >> 8) }
#define s16_from_packet_le(v) ((int16_t)((v[1] << 8) | v[0]))

#endif /* _SCRAW_PACKETS_H_ */
