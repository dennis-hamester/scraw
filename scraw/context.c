/* Copyright (c) 2016, Dennis Hamester <dennis.hamester@startmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include "context.h"
#include "controller.h"
#include "device.h"
#include <stdlib.h>
#include <string.h>

#define USB_ID_VENDOR_VALVE 0x28de
#define USB_ID_PRODUCT_WIRED 0x1102
#define USB_ID_PRODUCT_WIRELESS 0x1142

static void notify_controllers(scraw_context_t* ctx, device_t* dev, int reason) {
    for(int i = 0; i < dev->num_ctrls; ++i) {
        scraw_controller_t* ctrl = dev->ctrls[i];
        if(ctrl->connected) {
            ctx->event_cb(ctx, reason, ctrl, ctx->event_user_data);
        }
    }
}

void handle_controller_state_change_events(scraw_context_t* ctx) {
    for(int i = 0; i < SCRAW_MAX_NUM_DEVICES; ++i) {
        device_t* dev = ctx->devs[i];

        if(!dev) {
            continue;
        }

        for(int j = 0; j < dev->num_ctrls; ++j) {
            scraw_controller_t* ctrl = dev->ctrls[j];
            controller_raise_state_change(ctrl);
        }
    }
}

static void handle_controller_hotplug_events(scraw_context_t* ctx) {
    if(!ctx->event_cb) {
        return;
    }

    for(int i = 0; i < SCRAW_MAX_NUM_DEVICES; ++i) {
        device_t* dev = ctx->devs[i];

        if(!dev) {
            continue;
        }

        for(int j = 0; j < dev->num_ctrls; ++j) {
            scraw_controller_t* ctrl = dev->ctrls[j];

            if(ctrl->connected != ctrl->hotplug_status) {
                ctrl->hotplug_status = ctrl->connected;

                int reason;
                if(ctrl->connected) {
                    reason = SCRAW_EVENT_CONTROLLER_GAINED;
                }
                else {
                    reason = SCRAW_EVENT_CONTROLLER_LOST;
                }

                ctx->event_cb(ctx, reason, ctrl, ctx->event_user_data);
            }
        }
    }

}

static void handle_device_hotplug_events(scraw_context_t* ctx) {
    for(int i = 0; i < SCRAW_MAX_NUM_DEVICES; ++i) {
        libusb_device* usb_dev = ctx->new_devs[i];
        if(!usb_dev) {
            continue;
        }

        for(int j = 0; j < SCRAW_MAX_NUM_DEVICES; ++j) {
            if(ctx->devs[j] && (ctx->devs[j]->usb_dev == usb_dev)) {
                ctx->new_devs[i] = NULL;
                continue;
            }
        }

        struct libusb_device_descriptor desc;
        device_t* new_dev = NULL;

        if(libusb_get_device_descriptor(usb_dev, &desc) != LIBUSB_SUCCESS) {
            ctx->new_devs[i] = NULL;
            continue;
        }

        if(desc.idProduct == USB_ID_PRODUCT_WIRED) {
            /* Wired controller */
            device_create(&new_dev, ctx, usb_dev, SCRAW_CONTROLLER_TYPE_WIRED);
        }
        else if(desc.idProduct == USB_ID_PRODUCT_WIRELESS) {
            /* Wireless controller */
            device_create(&new_dev, ctx, usb_dev, SCRAW_CONTROLLER_TYPE_WIRELESS);
        }

        if(new_dev) {
            for(int j = 0; j < SCRAW_MAX_NUM_DEVICES; ++j) {
                if(!ctx->devs[j]) {
                    ctx->devs[j] = new_dev;
                    break;
                }
            }
        }

        ctx->new_devs[i] = NULL;
    }

    for(int i = 0; i < SCRAW_MAX_NUM_DEVICES; ++i) {
        libusb_device* usb_dev = ctx->lost_devs[i];

        if(!usb_dev) {
            continue;
        }

        device_t* dev = NULL;
        for(int j = 0; j < SCRAW_MAX_NUM_DEVICES; ++j) {
            if(ctx->devs[j] && (ctx->devs[j]->usb_dev == usb_dev)) {
                dev = ctx->devs[j];
                ctx->devs[j] = NULL;
                break;
            }
        }

        if(!dev) {
            ctx->lost_devs[i] = NULL;
            continue;
        }

        if(ctx->event_cb) {
            notify_controllers(ctx, dev, SCRAW_EVENT_CONTROLLER_LOST);
        }

        device_destroy(dev);
        ctx->lost_devs[i] = NULL;
    }
}

static int hotplug_callback(libusb_context* usb_ctx, libusb_device* usb_dev, libusb_hotplug_event event, void *user_data) {
    scraw_context_t* ctx = user_data;
    pthread_mutex_lock(&ctx->mutex);

    if(event == LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED) {
        for(int i = 0; i < SCRAW_MAX_NUM_DEVICES; ++i) {
            if(!ctx->new_devs[i]) {
                ctx->new_devs[i] = usb_dev;
                break;
            }
        }
    }
    else if(event == LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT) {
        for(int i = 0; i < SCRAW_MAX_NUM_DEVICES; ++i) {
            if(!ctx->lost_devs[i]) {
                ctx->lost_devs[i] = usb_dev;
                break;
            }
        }
    }

    pthread_mutex_unlock(&ctx->mutex);
    return 0;
}

int scraw_context_create(scraw_context_t** ctx, scraw_event_cb_t cb, void* user_data) {
    if(!libusb_has_capability(LIBUSB_CAP_HAS_HOTPLUG)) {
        return SCRAW_RESULT_ERROR_UNKNOWN;
    }

    libusb_context* usb_ctx = NULL;
    if(libusb_init(&usb_ctx) != LIBUSB_SUCCESS) {
        return SCRAW_RESULT_ERROR_UNKNOWN;
    }

    scraw_context_t* new_ctx = malloc(sizeof(scraw_context_t));
    pthread_mutex_init(&new_ctx->mutex, NULL);
    new_ctx->usb_ctx = usb_ctx;

    memset(new_ctx->devs, 0, sizeof(new_ctx->devs));
    memset(new_ctx->new_devs, 0, sizeof(new_ctx->new_devs));
    memset(new_ctx->lost_devs, 0, sizeof(new_ctx->lost_devs));
    new_ctx->event_cb = cb;
    new_ctx->event_user_data = user_data;

    libusb_hotplug_register_callback(new_ctx->usb_ctx,
                                     LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED | LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT,
                                     LIBUSB_HOTPLUG_ENUMERATE, USB_ID_VENDOR_VALVE,
                                     LIBUSB_HOTPLUG_MATCH_ANY, LIBUSB_HOTPLUG_MATCH_ANY,
                                     hotplug_callback, new_ctx, &new_ctx->usb_hotplug_handle);

    *ctx = new_ctx;
    return SCRAW_RESULT_SUCCESS;
}

void scraw_context_destroy(scraw_context_t* ctx) {
    libusb_hotplug_deregister_callback(ctx->usb_ctx, ctx->usb_hotplug_handle);

    if(ctx->event_cb) {
        handle_controller_hotplug_events(ctx);
        handle_device_hotplug_events(ctx);
        for(int i = 0; i < SCRAW_MAX_NUM_DEVICES; ++i) {
            device_t* dev = ctx->devs[i];
            if(dev) {
                notify_controllers(ctx, dev, SCRAW_EVENT_CONTROLLER_LOST);
            }
        }
    }

    for(int i = 0; i < SCRAW_MAX_NUM_DEVICES; ++i) {
        if(ctx->devs[i]) {
            device_destroy(ctx->devs[i]);
        }
    }

    libusb_exit(ctx->usb_ctx);
    pthread_mutex_destroy(&ctx->mutex);
    free(ctx);
}

void scraw_context_handle_events(scraw_context_t* ctx, long timeout_ms) {
    struct timeval tv = {
        .tv_sec = timeout_ms / 1000,
        .tv_usec = 1000 * (timeout_ms % 1000),
    };

    libusb_handle_events_timeout(ctx->usb_ctx, &tv);

    pthread_mutex_lock(&ctx->mutex);
    handle_controller_state_change_events(ctx);
    handle_device_hotplug_events(ctx);
    handle_controller_hotplug_events(ctx);
    pthread_mutex_unlock(&ctx->mutex);
}

void scraw_context_list_controllers(scraw_context_t* ctx, scraw_controller_t*** ctrls, int* num_ctrls) {
    scraw_controller_t** list = malloc((SCRAW_MAX_NUM_CONTROLLERS + 1) * sizeof(scraw_controller_t*));
    int num = 0;

    pthread_mutex_lock(&ctx->mutex);

    for(int i = 0; i < SCRAW_MAX_NUM_DEVICES; ++i) {
        device_t* dev = ctx->devs[i];
        if(!dev) {
            continue;
        }

        for(int j = 0; j < dev->num_ctrls; ++j) {
            scraw_controller_t* ctrl = dev->ctrls[j];
            if(ctrl->connected) {
                scraw_controller_retain(ctrl);
                list[num] = ctrl;
                ++num;
            }
        }
    }

    pthread_mutex_unlock(&ctx->mutex);

    list[num] = NULL;
    *ctrls = list;
    *num_ctrls = num;
}

void scraw_context_free_controller_list(scraw_controller_t** ctrls) {
    for(scraw_controller_t** iter = ctrls; *iter; ++iter) {
        scraw_controller_release(*iter);
    }

    free(ctrls);
}
