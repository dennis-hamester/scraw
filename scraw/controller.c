/* Copyright (c) 2016, Dennis Hamester <dennis.hamester@startmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include "controller.h"
#include "context.h"
#include "device.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAX_IO_TRIES 20
#define IO_WAIT_INIT_WIRED 5 * 1000
#define IO_WAIT_INIT_WIRELESS 25 * 1000
#define IO_WAIT_MULTIPLIER 1.1

#define USB_HID_REQUEST_GET_REPORT 0x01
#define USB_HID_REQUEST_SET_REPORT 0x09
#define USB_HID_REPORT_TYPE_FEATURE 0x0300

#define LEFT_TRACKPAD_AND_JOYSTICK ((uint32_t)0x00000080)

static void handle_state_update(scraw_controller_t* ctrl, packet_state_update* update) {
    uint32_t seq_number = u32_from_packet_le(update->seq_number);
    if(seq_number <= ctrl->seq_number) {
        return;
    }

    pthread_mutex_lock(&ctrl->mutex);

    ctrl->seq_number = seq_number;

    int state_id = ctrl->cur_state;
    scraw_controller_state_t* state = &ctrl->state[state_id].state;
    ctrl->state[state_id].changed = true;

    state->size = sizeof(scraw_controller_state_t);

    uint32_t buttons = (update->buttons[0] << 16) | (update->buttons[1] << 8) | update->buttons[2];
    state->buttons = buttons;
    if(buttons & LEFT_TRACKPAD_AND_JOYSTICK) {
        state->buttons &= ~LEFT_TRACKPAD_AND_JOYSTICK;
        state->buttons |= SCRAW_BTN_LEFT_TP_TOUCH;
    }

    if(ctrl->type == SCRAW_CONTROLLER_TYPE_WIRED) {
        state->left_trigger = s16_from_packet_le(update->left_trigger_full);
        state->right_trigger = s16_from_packet_le(update->right_trigger_full);
    }
    else {
        state->left_trigger = (0x7fff * update->left_trigger) / 0xff;
        state->right_trigger = (0x7fff * update->right_trigger) / 0xff;
    }

    int16_t left_analog_x = s16_from_packet_le(update->left_analog_x);
    int16_t left_analog_y = s16_from_packet_le(update->left_analog_y);
    if(buttons & SCRAW_BTN_LEFT_TP_TOUCH) {
        state->left_trackpad_x = left_analog_x;
        state->left_trackpad_y = left_analog_y;

        if(!(buttons & LEFT_TRACKPAD_AND_JOYSTICK)) {
            state->joystick_x = 0;
            state->joystick_y = 0;
        }
    }
    else {
        state->joystick_x = left_analog_x;
        state->joystick_y = left_analog_y;

        if(!(buttons & LEFT_TRACKPAD_AND_JOYSTICK)) {
            state->buttons &= ~SCRAW_BTN_LEFT_TP;
            state->left_trackpad_x = 0;
            state->left_trackpad_y = 0;
        }
    }

    state->right_trackpad_x = s16_from_packet_le(update->right_trackpad_x);
    state->right_trackpad_y = s16_from_packet_le(update->right_trackpad_y);

    state->d_imu_pitch = s16_from_packet_le(update->d_imu_pitch);
    state->d_imu_roll = s16_from_packet_le(update->d_imu_roll);
    state->d_imu_yaw = s16_from_packet_le(update->d_imu_yaw);

    pthread_mutex_unlock(&ctrl->mutex);
}

static void handle_packet_state(scraw_controller_t* ctrl) {
    packet_state* state = &ctrl->int_packet.state;

    switch(state->sub_type) {
    case PACKET_STATE_ST_UPDATE:
        assert(state->size == 0x3c);
        handle_state_update(ctrl, &state->update);
        break;

    case PACKET_STATE_ST_SEQ:
        assert(state->size == 0x0b);
        break;

    case PACKET_STATE_ST_HOTPLUG:
        assert(state->size == 0x01);
        ctrl->connected = state->hotplug.status == PACKET_PROBE_STATUS_CONNECTED;
        break;

    default:
        break;
    }
}

static void int_callback(struct libusb_transfer* transfer) {
    scraw_controller_t* ctrl = transfer->user_data;

    if(transfer->status == LIBUSB_TRANSFER_TIMED_OUT) {
        libusb_submit_transfer(transfer);
        return;
    }

    if(transfer->status != LIBUSB_TRANSFER_COMPLETED) {
        ctrl->transfer_destroyed = 1;
        return;
    }

    switch(ctrl->int_packet.type) {
    case PACKET_TYPE_STATE:
        handle_packet_state(ctrl);
        break;

    default:
        break;
    }

    libusb_submit_transfer(transfer);
}

static int write_feature_report(scraw_controller_t* ctrl, uint8_t* data, uint16_t size) {
    return libusb_control_transfer(ctrl->dev->usb_handle,
                                   LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE,
                                   USB_HID_REQUEST_SET_REPORT,
                                   USB_HID_REPORT_TYPE_FEATURE,
                                   ctrl->usb_interface, data, size, 1000);
}

static int read_feature_report(scraw_controller_t* ctrl, uint8_t* data, uint16_t size) {
    return libusb_control_transfer(ctrl->dev->usb_handle,
                                   LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE,
                                   USB_HID_REQUEST_GET_REPORT,
                                   USB_HID_REPORT_TYPE_FEATURE,
                                   ctrl->usb_interface, data, size, 1000);
}

static int write_config(scraw_controller_t* ctrl, packet* pkt) {
    int usb_res;
    int tries = 0;
    pthread_mutex_lock(&ctrl->rw_mutex);

    while(true) {
        usb_res = write_feature_report(ctrl, pkt->data, sizeof(packet));
        if(usb_res == LIBUSB_ERROR_PIPE) {
            if(tries >= MAX_IO_TRIES) {
                break;
            }
            else if(tries > 0) {
                ctrl->io_wait *= IO_WAIT_MULTIPLIER;
            }

            usleep(ctrl->io_wait);
            ++tries;
        }
        else {
            break;
        }
    };

    pthread_mutex_unlock(&ctrl->rw_mutex);

    if(usb_res == sizeof(packet)) {
        return SCRAW_RESULT_SUCCESS;
    }
    else {
        return SCRAW_RESULT_ERROR_UNKNOWN;
    }
}

static int read_config(scraw_controller_t* ctrl, packet* pkt, uint8_t expected_size) {
    packet out = *pkt;
    int usb_res;
    int res;
    int tries = 0;
    pthread_mutex_lock(&ctrl->rw_mutex);

    while(true) {
        res = write_config(ctrl, &out);

        if(res == SCRAW_RESULT_SUCCESS) {
            usleep(ctrl->io_wait);
            usb_res = read_feature_report(ctrl, pkt->data, sizeof(packet));

            if(usb_res != sizeof(packet)) {
                res = SCRAW_RESULT_ERROR_UNKNOWN;
                break;
            }

            if(pkt->data[1] == expected_size) {
                res = SCRAW_RESULT_SUCCESS;
                break;
            }
            else if(tries >= MAX_IO_TRIES) {
                res = SCRAW_RESULT_ERROR_UNKNOWN;
                break;
            }

            ctrl->io_wait *= IO_WAIT_MULTIPLIER;
            ++tries;
        }
        else {
            res = SCRAW_RESULT_ERROR_UNKNOWN;
            break;
        }
    }

    pthread_mutex_unlock(&ctrl->rw_mutex);
    return res;
}

static bool probe_controller(scraw_controller_t* ctrl) {
    if(ctrl->type == SCRAW_CONTROLLER_TYPE_WIRED) {
        return true;
    }

    packet_probe probe = {
        .type = PACKET_TYPE_PROBE,
        .size = 0x00,
    };

    if(read_config(ctrl, (packet*)&probe, 1) == SCRAW_RESULT_SUCCESS) {
        assert(probe.type == PACKET_TYPE_PROBE);
        assert(probe.size == 1);
        return probe.status == PACKET_PROBE_STATUS_CONNECTED;
    }

    return false;
}

static int read_id(scraw_controller_t* ctrl, scraw_controller_info_t* info) {
    int res;
    packet_id id = {
        .type = PACKET_TYPE_ID,
        .size = 0,
    };

    if(ctrl->type == SCRAW_CONTROLLER_TYPE_WIRED) {
        /* Reading this the first time from a wired controller doesn't contain
         * the radio timestamp. You can also see this happen in the Steam
         * client. Work around by reading twice.
         */
        res = read_config(ctrl, (packet*)&id, 0x23);
        if(res != SCRAW_RESULT_SUCCESS) {
            return res;
        }
    }

    res = read_config(ctrl, (packet*)&id, 0x23);
    if(res == SCRAW_RESULT_SUCCESS) {
        info->product_id = u16_from_packet_le(id.product);
        info->bootloader_ts = u32_from_packet_le(id.bootloader_ts);
        info->firmware_ts = u32_from_packet_le(id.firmware_ts);
        info->radio_ts = u32_from_packet_le(id.radio_ts);
    }

    return res;
}

static int read_serial_number(scraw_controller_t* ctrl, scraw_controller_info_t* info) {
    packet_serial_number sn = {
        .type = PACKET_TYPE_SERIAL_NUMBER,
        .size = 0x15,
        .id = PACKET_SERIAL_NUMBER_ID_CONTROLLER,
    };

    int res = read_config(ctrl, (packet*)&sn, 0x15);
    if(res == SCRAW_RESULT_SUCCESS) {
        memcpy(&info->serial_number, sn.serial, 10);
        info->serial_number[10] = 0;
    }

    return res;
}

static int read_receiver_id(scraw_controller_t* ctrl, scraw_controller_info_t* info) {
    packet_receiver_id id = {
        .type = PACKET_TYPE_RECEIVER_ID,
        .size = 0,
    };

    int res = read_config(ctrl, (packet*)&id, 0x22);
    if(res == SCRAW_RESULT_SUCCESS) {
        info->receiver_firmware_ts = u32_from_packet_be(id.firmware_ts);
        memcpy(&info->receiver_serial_number, id.serial, 10);
        info->receiver_serial_number[10] = 0;
    }

    return res;
}

int controller_create(scraw_controller_t** ctrl, device_t* dev, int interface) {
    int res = device_claim_interface(dev, interface);
    if(res != SCRAW_RESULT_SUCCESS) {
        return res;
    }

    scraw_controller_t* new_ctrl = malloc(sizeof(scraw_controller_t));
    new_ctrl->type = dev->type;

    pthread_mutexattr_t attrs;
    pthread_mutexattr_init(&attrs);
    pthread_mutexattr_settype(&attrs, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init(&new_ctrl->mutex, &attrs);
    pthread_mutex_init(&new_ctrl->rw_mutex, &attrs);
    pthread_mutexattr_destroy(&attrs);

    new_ctrl->ref_count = 0;
    new_ctrl->io_wait = (dev->type == SCRAW_CONTROLLER_TYPE_WIRED) ? IO_WAIT_INIT_WIRED : IO_WAIT_INIT_WIRELESS;
    new_ctrl->dev = dev;
    new_ctrl->usb_interface = interface;
    new_ctrl->usb_int_endpoint = LIBUSB_ENDPOINT_IN | (interface + 1);
    new_ctrl->transfer = libusb_alloc_transfer(0);
    new_ctrl->transfer_destroyed = 0;
    new_ctrl->seq_number = 0;
    new_ctrl->cur_state = 0;
    new_ctrl->state_cb = NULL;
    new_ctrl->user_data = NULL;
    new_ctrl->destroy_cb = NULL;

    libusb_fill_interrupt_transfer(new_ctrl->transfer, new_ctrl->dev->usb_handle, new_ctrl->usb_int_endpoint, (unsigned char*)&new_ctrl->int_packet, sizeof(new_ctrl->int_packet), int_callback, new_ctrl, 1500);
    libusb_submit_transfer(new_ctrl->transfer);

    new_ctrl->connected = probe_controller(new_ctrl);
    new_ctrl->hotplug_status = false;

    *ctrl = new_ctrl;
    return SCRAW_RESULT_SUCCESS;
}

void controller_destroy(scraw_controller_t* ctrl) {
    pthread_mutex_lock(&ctrl->mutex);

    if(ctrl->dev) {
        while(!ctrl->transfer_destroyed) {
            libusb_cancel_transfer(ctrl->transfer);
            libusb_handle_events_completed(ctrl->dev->ctx->usb_ctx, &ctrl->transfer_destroyed);
        }

        device_release_interface(ctrl->dev, ctrl->usb_interface);
    }

    if(ctrl->ref_count == 0) {
        pthread_mutex_unlock(&ctrl->mutex);
        pthread_mutex_destroy(&ctrl->mutex);
        pthread_mutex_destroy(&ctrl->rw_mutex);
        libusb_free_transfer(ctrl->transfer);
        free(ctrl);
    }
    else {
        ctrl->connected = false;
        ctrl->dev = NULL;
        pthread_mutex_unlock(&ctrl->mutex);
    }
}

void scraw_controller_retain(scraw_controller_t* ctrl) {
    pthread_mutex_lock(&ctrl->mutex);

    ++ctrl->ref_count;
    if(ctrl->ref_count == 1) {
        ctrl->state[0].changed = false;
        ctrl->state[1].changed = false;
    }

    pthread_mutex_unlock(&ctrl->mutex);
}

void scraw_controller_release(scraw_controller_t* ctrl) {
    pthread_mutex_lock(&ctrl->mutex);
    assert(ctrl->ref_count > 0);

    --ctrl->ref_count;
    if(ctrl->ref_count == 0) {
        if(ctrl->destroy_cb) {
            ctrl->destroy_cb(ctrl);
        }

        ctrl->state_cb = NULL;
        ctrl->user_data = NULL;
        ctrl->destroy_cb = NULL;
        pthread_mutex_unlock(&ctrl->mutex);

        if(!ctrl->dev) {
            controller_destroy(ctrl);
        }
    }
    else {
        pthread_mutex_unlock(&ctrl->mutex);
    }
}

int scraw_controller_on_destroy(scraw_controller_t* ctrl, scraw_controller_destroy_cb_t cb) {
    if(!ctrl->connected) {
        return SCRAW_RESULT_ERROR_DISCONNECTED;
    }

    pthread_mutex_lock(&ctrl->mutex);

    ctrl->destroy_cb = cb;

    pthread_mutex_unlock(&ctrl->mutex);
    return SCRAW_RESULT_SUCCESS;
}

void controller_raise_state_change(scraw_controller_t* ctrl) {
    pthread_mutex_lock(&ctrl->mutex);
    if(!ctrl->connected || !ctrl->state_cb) {
        pthread_mutex_unlock(&ctrl->mutex);
        return;
    }

    int state_id = ctrl->cur_state;
    ctrl->cur_state = 1 - state_id;

    if(ctrl->state[state_id].changed) {
        ctrl->state[state_id].changed = false;
        ctrl->state_cb(ctrl, &ctrl->state[state_id].state);
    }

    pthread_mutex_unlock(&ctrl->mutex);
}

int scraw_controller_set_user_data(scraw_controller_t* ctrl, void* user_data) {
    if(!ctrl->connected) {
        return SCRAW_RESULT_ERROR_DISCONNECTED;
    }

    pthread_mutex_lock(&ctrl->mutex);

    ctrl->user_data = user_data;

    pthread_mutex_unlock(&ctrl->mutex);
    return SCRAW_RESULT_SUCCESS;
}

int scraw_controller_get_user_data(scraw_controller_t* ctrl, void** user_data) {
    *user_data = ctrl->user_data;
    return SCRAW_RESULT_SUCCESS;
}

int scraw_controller_is_available(scraw_controller_t* ctrl) {
    if(ctrl->connected) {
        return SCRAW_RESULT_SUCCESS;
    }
    else {
        return SCRAW_RESULT_ERROR_DISCONNECTED;
    }
}

int scraw_controller_get_type(scraw_controller_t* ctrl, int* type) {
    *type = ctrl->type;
    return SCRAW_RESULT_SUCCESS;
}

int scraw_controller_get_info(scraw_controller_t* ctrl, scraw_controller_info_t* info) {
    scraw_controller_info_t tmp;
    memset(&tmp, 0, sizeof(scraw_controller_info_t));
    tmp.size = sizeof(scraw_controller_info_t) < info->size ? sizeof(scraw_controller_t) : info->size;
    tmp.type = ctrl->type;
    int res;

    res = read_id(ctrl, &tmp);
    if(res != SCRAW_RESULT_SUCCESS) {
        return res;
    }

    res = read_serial_number(ctrl, &tmp);
    if(res != SCRAW_RESULT_SUCCESS) {
        return res;
    }

    if(ctrl->type == SCRAW_CONTROLLER_TYPE_WIRELESS) {
        res = read_receiver_id(ctrl, &tmp);
        if(res != SCRAW_RESULT_SUCCESS) {
            return res;
        }
    }

    memcpy(info, &tmp, tmp.size);
    return SCRAW_RESULT_SUCCESS;
}

int scraw_controller_on_state_change(scraw_controller_t* ctrl, scraw_state_change_cb_t cb) {
    if(!ctrl->connected) {
        return SCRAW_RESULT_ERROR_DISCONNECTED;
    }

    pthread_mutex_lock(&ctrl->mutex);

    ctrl->state_cb = cb;

    pthread_mutex_unlock(&ctrl->mutex);
    return SCRAW_RESULT_SUCCESS;
}

int scraw_controller_get_state(scraw_controller_t* ctrl, scraw_controller_state_t* state, bool* changed) {
    if(!ctrl->connected) {
        return SCRAW_RESULT_ERROR_DISCONNECTED;
    }

    pthread_mutex_lock(&ctrl->mutex);

    int state_id = ctrl->cur_state;

    if(!changed || ctrl->state[state_id].changed) {
        size_t size = sizeof(scraw_controller_state_t) < state->size ? sizeof(scraw_controller_state_t) : state->size;
        memcpy(state, &ctrl->state, size);
    }

    if(changed) {
        *changed = ctrl->state[state_id].changed;
    }

    ctrl->state[state_id].changed = false;

    pthread_mutex_unlock(&ctrl->mutex);
    return SCRAW_RESULT_SUCCESS;
}

int scraw_controller_configure(scraw_controller_t* ctrl, scraw_controller_config_t* config) {
    if(!ctrl->connected) {
        return SCRAW_RESULT_ERROR_DISCONNECTED;
    }

    assert(config->size == sizeof(scraw_controller_config_t));

    packet_configure cfg = {
        .type = PACKET_TYPE_CONFIGURE,
        .size = 0x15,
        .unknown_02 = 0x32,
        .cfg = {
            .timeout = u16_to_packet_le(config->idle_timeout),
            .unknown_05_12 = { 0x18, 0x00, 0x00, 0x31, 0x02, 0x00, 0x08, 0x07, 0x00, 0x07, 0x07, 0x00, 0x30 },
            .enable_imu = (config->imu ? 0x14 : 0x00 ),
            .unknown_14_15 = { 0x00, 0x2e },
            .unused = { 0 },
        },
    };

    return write_config(ctrl, (packet*)&cfg);
}

int scraw_controller_lizard_buttons(scraw_controller_t* ctrl, bool enable) {
    if(!ctrl->connected) {
        return SCRAW_RESULT_ERROR_DISCONNECTED;
    }

    if(enable) {
        packet_enable_lizard_buttons btn = {
            .type = PACKET_TYPE_ENABLE_LIZARD_BUTTONS,
            .size = 0x00,
        };

        return write_config(ctrl, (packet*)&btn);
    }
    else {
        packet_disable_lizard_buttons btn = {
            .type = PACKET_TYPE_DISABLE_LIZARD_BUTTONS,
            .size = 0x00,
        };

        return write_config(ctrl, (packet*)&btn);
    }
}

int scraw_controller_enable_lizard_analog(scraw_controller_t* ctrl) {
    if(!ctrl->connected) {
        return SCRAW_RESULT_ERROR_DISCONNECTED;
    }

    packet_enable_lizard_axes axes = {
        .type = PACKET_TYPE_ENABLE_LIZARD_AXES,
        .size = 0x00,
    };

    return write_config(ctrl, (packet*)&axes);
}

int scraw_controller_feedback(scraw_controller_t* ctrl, uint8_t side, uint16_t amplitude, uint16_t period, uint16_t count) {
    if(!ctrl->connected) {
        return SCRAW_RESULT_ERROR_DISCONNECTED;
    }

    packet_feedback hap = {
        .type = PACKET_TYPE_FEEDBACK,
        .size = 0x08,
        .side = side,
        .amplitude = u16_to_packet_le(amplitude),
        .period = u16_to_packet_le(period),
        .count = u16_to_packet_le(count),
        .unknown_09 = 0x00,
    };

    return write_config(ctrl, (packet*)&hap);
}

int scraw_controller_set_led_brightness(scraw_controller_t* ctrl, uint8_t percent) {
    if(!ctrl->connected) {
        return SCRAW_RESULT_ERROR_DISCONNECTED;
    }

    if(percent > 100) {
        percent = 100;
    }

    packet_configure cfg = {
        .type = PACKET_TYPE_CONFIGURE,
        .size = 0x03,
        .unknown_02 = 0x2d,
        .led = {
            .led = percent,
            .unknown_04 = 0x00,
        },
    };

    return write_config(ctrl, (packet*)&cfg);
}

int scraw_controller_set_audio_indices(scraw_controller_t* ctrl, uint8_t* indices) {
    if(!ctrl->connected) {
        return SCRAW_RESULT_ERROR_DISCONNECTED;
    }

    packet_set_audio_indices idx = {
        .type = PACKET_TYPE_SET_AUDIO_INDICES,
        .size = 0x10,
    };

    memcpy(&idx.indices, indices, 16);

    return write_config(ctrl, (packet*)&idx);
}

int scraw_controller_play_audio(scraw_controller_t* ctrl, uint8_t index) {
    if(!ctrl->connected) {
        return SCRAW_RESULT_ERROR_DISCONNECTED;
    }

    packet_audio audio = {
        .type = PACKET_TYPE_AUDIO,
        .size = 0x04,
        .audio_type = index,
        .unknown_03_05 = { 0 },
    };

    return write_config(ctrl, (packet*)&audio);
}

int scraw_controller_turn_off(scraw_controller_t* ctrl) {
    if(!ctrl->connected) {
        return SCRAW_RESULT_ERROR_DISCONNECTED;
    }

    if(ctrl->type != SCRAW_CONTROLLER_TYPE_WIRELESS) {
        return SCRAW_RESULT_NOT_SUPPORTED;
    }

    packet_off off = {
        .type = PACKET_TYPE_OFF,
        .size = 0x04,
        .off = { 0x6f, 0x66, 0x66, 0x21 } /* "off!" */
    };

    return write_config(ctrl, (packet*)&off);
}

int scraw_controller_calibrate(scraw_controller_t* ctrl, int component) {
    if(!ctrl->connected) {
        return SCRAW_RESULT_ERROR_DISCONNECTED;
    }

    packet pkt = {
        .data = { 0 }
    };

    switch(component) {
    case SCRAW_CALIBRATE_TRACKPAD:
        pkt.calibrate_trackpad.type = PACKET_TYPE_CALIBRATE_TRACKPAD;
        break;

    case SCRAW_CALIBRATE_JOYSTICK:
        pkt.calibrate_joystick.type = PACKET_TYPE_CALIBRATE_JOYSTICK;
        break;

    case SCRAW_CALIBRATE_IMU:
        pkt.calibrate_imu.type = PACKET_TYPE_CALIBRATE_IMU;
        break;

    default:
        return SCRAW_RESULT_NOT_SUPPORTED;
    }

    return write_config(ctrl, &pkt);
}
