/* Copyright (c) 2016, Dennis Hamester <dennis.hamester@startmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef _SCRAW_CONTEXT_H_
#define _SCRAW_CONTEXT_H_

#include <scraw/scraw.h>
#include <pthread.h>
#include <libusb.h>

#define SCRAW_MAX_NUM_DEVICES 16
#define SCRAW_MAX_NUM_CONTROLLERS (4 * SCRAW_MAX_NUM_DEVICES)

typedef struct device device_t;

struct scraw_context {
    pthread_mutex_t mutex;
    libusb_context* usb_ctx;
    libusb_hotplug_callback_handle usb_hotplug_handle;
    device_t* devs[SCRAW_MAX_NUM_DEVICES];
    libusb_device* new_devs[SCRAW_MAX_NUM_DEVICES];
    libusb_device* lost_devs[SCRAW_MAX_NUM_DEVICES];
    scraw_event_cb_t event_cb;
    void* event_user_data;
};

#endif /* _SCRAW_CONTEXT_H_ */
