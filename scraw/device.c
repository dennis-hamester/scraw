/* Copyright (c) 2016, Dennis Hamester <dennis.hamester@startmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include "device.h"
#include "controller.h"
#include <stdlib.h>

int device_claim_interface(device_t* dev, int interface) {
    int usb_res = libusb_kernel_driver_active(dev->usb_handle, interface);
    if(usb_res == 1) {
        usb_res = libusb_detach_kernel_driver(dev->usb_handle, interface);
        if(usb_res != LIBUSB_SUCCESS) {
            return SCRAW_RESULT_ERROR_UNKNOWN;
        }
    }
    else if((usb_res != 0) && (usb_res != LIBUSB_ERROR_NOT_SUPPORTED)) {
        return SCRAW_RESULT_ERROR_UNKNOWN;
    }

    usb_res = libusb_claim_interface(dev->usb_handle, interface);
    if(usb_res != LIBUSB_SUCCESS) {
        return SCRAW_RESULT_ERROR_UNKNOWN;
    }

    return SCRAW_RESULT_SUCCESS;
}

void device_release_interface(device_t* dev, int interface) {
    if(libusb_release_interface(dev->usb_handle, interface) == LIBUSB_SUCCESS) {
        libusb_attach_kernel_driver(dev->usb_handle, interface);
    }
}

int device_create(device_t** dev, scraw_context_t* ctx, libusb_device* usb_dev, int type) {
    libusb_device_handle* usb_handle;
    if(libusb_open(usb_dev, &usb_handle) != LIBUSB_SUCCESS) {
        return SCRAW_RESULT_ERROR_UNKNOWN;
    }

    device_t* new_dev = malloc(sizeof(device_t));
    new_dev->ctx = ctx;
    new_dev->usb_dev = usb_dev;
    new_dev->usb_handle = usb_handle;
    new_dev->type = type;
    int first_usb_interface;
    if(type == SCRAW_CONTROLLER_TYPE_WIRED) {
        new_dev->num_ctrls = 1;
        first_usb_interface = 2;
    }
    else {
        new_dev->num_ctrls = 4;
        first_usb_interface = 1;
    }

    new_dev->ctrls = calloc(new_dev->num_ctrls, sizeof(scraw_controller_t*));
    for(int i = 0; i < new_dev->num_ctrls; ++i) {
        int res = controller_create(&new_dev->ctrls[i], new_dev, first_usb_interface + i);
        if(res != SCRAW_RESULT_SUCCESS) {
            device_destroy(new_dev);
            return res;
        }
    }

    *dev = new_dev;

    return SCRAW_RESULT_SUCCESS;
}

void device_destroy(device_t* dev) {
    for(int i = 0; i < dev->num_ctrls; ++i) {
        if(dev->ctrls[i]) {
            controller_destroy(dev->ctrls[i]);
        }
    }

    libusb_close(dev->usb_handle);
    free(dev->ctrls);
    free(dev);
}
