This directory contains the protocol description. 

A rendered version is available at
https://dennis-hamester.gitlab.io/scraw/protocol/. It uses the
[readtheorg](https://github.com/fniessen/org-html-themes) theme by Fabrice
Niessen, which is licensed under the GPL-3.0.
