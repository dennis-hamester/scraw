#!/usr/bin/emacs --script

(require 'ox-html)

(find-file "protocol.org")
(setq org-export-with-section-numbers nil)
(org-html-export-to-html)
