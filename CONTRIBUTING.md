Contributing
============
The following is just a loose list of things where `scraw` could need some help.
If you have any other ideas, let me know by opening an issue or a merge request.


Documentation
=============
* Even though all functions are documented, there is still lots of things to
  document. Especially the main Doxygen pages which can be found [here](doc)
  (files ending in `.dox`).

* Multi-threading and some restrictions that come with it needs to be
  documented.
  

Testing
=======
These things work only in theory and have never been tested fully:
* Using multiple controllers at the same time on a single dongle.

* Using multiple dongles and or multiple wired controllers.

* Multi-threading. `scraw` was developed with multi-threading in mind. I am
  probably even a bit too conservative when it comes to locking. But I haven't
  tested it extensively yet.


Protocol
========
The protocol used for communicating with a controller is documented in
[protocol/protocol.org](protocol.org). Please contribute if you know anything.
