# Copyright (c) 2016, Dennis Hamester <dennis.hamester@startmail.com>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
# FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.

find_package(PkgConfig)
pkg_check_modules(PC_libusb-1.0 libusb-1.0)

find_path(libusb-1.0_INCLUDE_DIR
  NAMES libusb.h
  PATHS ${PC_libusb-1.0_INCLUDE_DIRS}
)

find_library(libusb-1.0_LIBRARY
  NAMES usb-1.0
  PATHS ${PC_libusb-1.0_LIBRARY_DIRS}
)

set(libusb-1.0_VERSION ${PC_libusb-1.0_VERSION})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(libusb-1.0
  FOUND_VAR libusb-1.0_FOUND
  REQUIRED_VARS
    libusb-1.0_LIBRARY
    libusb-1.0_INCLUDE_DIR
  VERSION_VAR libusb-1.0_VERSION
)

if(libusb-1.0_FOUND AND NOT TARGET libusb-1.0::libusb-1.0)
  add_library(libusb-1.0::libusb-1.0 UNKNOWN IMPORTED)
  set_target_properties(libusb-1.0::libusb-1.0 PROPERTIES
    IMPORTED_LOCATION "${libusb-1.0_LIBRARY}"
    INTERFACE_COMPILE_OPTIONS "${PC_libusb-1.0_CFLAGS_OTHER}"
    INTERFACE_INCLUDE_DIRECTORIES "${libusb-1.0_INCLUDE_DIR}"
    )
endif()
