scraw Changelog
===============

Current git master
------------------
 * Correct IMU roll and yaw angular frequencies.

Version 0.2.1, released on 23.04.2016
-------------------------------------
 * Fix a deadlock in handle_state_update

Version 0.2.0, released on 23.03.2016
-------------------------------------
This release breaks API and binary compatibility with version 0.1.0.

* It is now okay to call any `scraw_controller_` function inside the state
  change callback.
* Added `scraw_controller_on_destroy`. It can be used to register a callback,
  which will be called when the reference counter reaches 0.
* Renamed enum `scraw_device_type` to `scraw_controller_type`.
* Prefix all `scraw_context_t` functions with `scraw_context_`.
* Prefix all `scraw_controller_t` functions with `scraw_controller_`.

Version 0.1.0, released on 17.03.2016
-------------------------------------
First release with the following features:

* Fully asynchronous hot-plugging.
* Asynchronous and synchronous access to all discrete buttons, track pads, the
  joystick as well as the IMU (inertial measurement unit).
* Enable and disable lizard mode.
* Serial number and build timestamps for controller and receiver can be queried.
* Simultaneous use of the left track pad and joystick.
* IMU can be enabled and disabled.
* Idle timeout after which the wireless controller automatically turns off can
  be configured.
* Haptic feedback supported.
* LED brightness can be controlled.
* Turn on and off sound can be changed.
* Various other audio tunes can be played at random.
* Controller calibration (track pads, joystick and IMU)
